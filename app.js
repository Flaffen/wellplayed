const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const createError = require('http-errors');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

require('./models');

const app = express();
const apiRouter = require('./routes/api');

mongoose.connect(process.env.MONGODB_URI, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log('Successfully connected to the database ' + process.env.MONGODB_URI);
  }
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api', apiRouter);

app.use(function(req, res, next) {
  next(createError(404));
});
app.use(function(err, req, res, next) {
  // res.status(err.status || 500);
  res.json({
    success: false,
    message: err.message
  });
});

const server = http.createServer(app);

require('./io.js')(server);

server.listen(3001);
