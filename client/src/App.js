import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      nickname: '',
      email: '',
      password: '',
      password_conf: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleNicknameChange = this.handleNicknameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePasswordConfirmationChange = this.handlePasswordConfirmationChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    
    if (this.state.password !== this.state.password_conf) {
      alert('Passwords do not match!');
      return;
    }

    fetch('/api/auth', {
      method: 'POST',
      body: JSON.stringify(this.state),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      alert('AAAANNND');
      alert(data.success);
    });
  }

  handleFirstNameChange(event) {
    this.setState({
      first_name: event.target.value
    });
  }

  handleLastNameChange(event) {
    this.setState({
      last_name: event.target.value
    });
  }


  handleNicknameChange(event) {
    this.setState({
      nickname: event.target.value
    });
  }


  handleEmailChange(event) {
    this.setState({
      email: event.target.value
    });
  }


  handlePasswordChange(event) {
    this.setState({
      password: event.target.value
    });
  }


  handlePasswordConfirmationChange(event) {
    this.setState({
      password_conf: event.target.value
    });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="first_name" value={this.state.first_name} onChange={this.handleFirstNameChange} />
          <input type="text" name="last_name" value={this.state.last_name} onChange={this.handleLastNameChange} />
          <input type="text" name="nickname" value={this.state.nickname} onChange={this.handleNicknameChange} />
          <input type="text" name="email" value={this.state.email} onChange={this.handleEmailChange} />
          <input type="text" name="password" value={this.state.password} onChange={this.handlePasswordChange} />
          <input type="text" name="password_conf" value={this.state.password_conf} onChange={this.handlePasswordConfirmationChange} />
          <button onClick={this.handleSubmit}>Register</button>
        </form>
      </div>
    );
  }
}

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    // TODO: Login
    // fetch('/api/auth/token?email=' + this.state.email + '&password=' + this.state.password)
    // .then()
  }

  handleEmailChange(event) {
    this.setState({
      email: event.target.value
    });
  }


  handlePasswordChange(event) {
    this.setState({
      password: event.target.value
    });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="email" value={this.state.email} onChange={this.handleEmailChange} />
          <input type="text" name="password" value={this.state.password} onChange={this.handlePasswordChange} />
          <button onClick={this.handleSubmit}>Log In</button>
        </form>
      </div>
    );
  }
}

class App extends Component {
  componentDidMount() {
    // fetch('/api/users?token=' + document.cookie.slice(document.cookie.indexOf('=') + 1))
    //   .then(res => res.json())
    //   .then(data => {
    //     console.log(data);
    //     alert(data.success);
    //   });
    // fetch('/api/auth/token?email=johndoe@example.com&password=12345')
    //   .then(res => res.json())
    //   .then(data => {
    //     if (data.success) {
    //       document.cookie = 'jwt_token=' + data.token;
    //     }
    //   });
  }

  render() {
    return (
      <div>
        <Link to="/">Register</Link> 
        <Link to="/login">Login</Link>

        <Route exact path="/" component={Register} />
        <Route path="/login" component={Login} />
      </div>
    );
  }
}

export default App;
