import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import RegistrationForm from './components/registration';
import LoginForm from './components/login';
import LogRegHeader from './components/logregheader';
import LogRegFooter from './components/logregfooter';
import './styles/style.css';

class LogReg extends Component {
  render() {
    return (
      <div>
        <LogRegHeader />

        <Route exact path="/" render={() => <LoginForm onLogin={this.props.onLogin}/>}/>
        <Route path="/reg" render={() => <RegistrationForm onLogin={this.props.onLogin}/>}/>

        <LogRegFooter />
      </div>
    );
  }
}

export default LogReg;
