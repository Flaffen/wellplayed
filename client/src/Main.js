import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

class Main extends Component {
  componentDidMount() {
    fetch('/api/users?token=' + document.cookie.slice(document.cookie.indexOf('=') + 1))
      .then(res => res.json())
      .then(data => {
        console.log(data);
      });
  }

  render() {
  	return <h1>Main Page</h1>
  }
}

export default Main;
