import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      redirect: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    fetch('/api/auth/token?email=' + this.state.email + '&password=' + this.state.password)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data.success) {
          document.cookie='jwt_token=' + data.token;
          this.props.onLogin();
        } else {
          alert('Email or password is wrong');
        }
      });
  }

  handleEmailChange(event) {
    this.setState({
      email: event.target.value
    });
  }

  handlePasswordChange(event) {
    this.setState({
      password: event.target.value
    });
  }

  render() {
    if (this.state.redirect) {
      return (<Redirect to="/"/>);
    } else {
    return (
      <main className="row">
        <div className="reg-form col-lg-3">
            <form className="reg-form-fields" action="" method="post">
                <label htmlFor="user-email">Адрес эл.почты:</label>
                <input className="reg-form-item" type="email" id="user-email" placeholder="Введите e-mail" value={this.state.email} onChange={this.handleEmailChange}/><br />
                <label htmlFor="user-pass">Пароль:</label>
                <input className="reg-form-item" type="password" id="user-pass" placeholder="Введите пароль" value={this.state.password} onChange={this.handlePasswordChange}/><br />
                <input className="reg-form-item-btn" type="submit" id="send" name="send" value="Вход" onClick={this.handleSubmit}/>
            </form>
        </div>
      </main>
    );
  }
  }
}

export default LoginForm;
