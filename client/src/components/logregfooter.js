import React, { Component } from 'react';

class LogRegFooter extends Component {
  render() {
    return (
      <footer className="main-footer" id="footer">
        <div className="main-footer-sections container-fluid">
          <div className="main-footer-sections__item row">
            <div className="col-lg-6" style={{fontSize: '2em'}}>Well Played Sports inc.</div>
            <div className="col-lg-6">
              <div className="social-menu">
                <a target="_blank" href="https://www.facebook.com/WellPlayedSports/"><i className="fa fa-facebook fa-lg" aria-hidden="true" /></a>
                <a target="_blank" href="https://medium.com"><i className="fa fa-medium fa-lg" aria-hidden="true" /></a>
                <a target="_blank" href="https://www.instagram.com/"><i className="fa fa-instagram fa-lg" aria-hidden="true" /></a>
                <a target="_blank" href="https://www.youtube.com/watch?v=wXsbUJdgTbg"><i className="fa fa-youtube-play fa-lg" aria-hidden="true" /></a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default LogRegFooter;
