import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class LogRegHeader extends Component {
  render() {
    return (
      <header className="main-header">
        <div className="main-menu container-fluid">
          <div className="main-menu-logo ">
            <p className="about-platform__text1" style={{position: 'relative', top: 15}}>Регистрация<span style={{color: 'red'}}>.</span></p>
          </div>
          <ul className="main-menu-nav">
            <li className="main-menu-nav__button">
              <Link to="/reg">Регистрация</Link>
            </li>
            <li className="main-menu-nav__button">
              <Link to="/">Вход</Link>
            </li>
          </ul>
        </div>
      </header>
    );
  }
}

export default LogRegHeader;
