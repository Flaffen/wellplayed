import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class RegistrationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      nickname: '',
      email: '',
      password: '',
      password_conf: '',
      redirect: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleNicknameChange = this.handleNicknameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePasswordConfirmationChange = this.handlePasswordConfirmationChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    
    if (this.state.password !== this.state.password_conf) {
      alert('Passwords do not match!');
      return;
    }

    fetch('/api/auth', {
      method: 'POST',
      body: JSON.stringify(this.state),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data.success) {
        document.cookie = 'jwt_token=' + data.token;
        this.props.onLogin();
      } else {
        alert('Error');
      }
    });
  }

  handleFirstNameChange(event) {
    this.setState({
      first_name: event.target.value
    });
  }

  handleLastNameChange(event) {
    this.setState({
      last_name: event.target.value
    });
  }


  handleNicknameChange(event) {
    this.setState({
      nickname: event.target.value
    });
  }


  handleEmailChange(event) {
    this.setState({
      email: event.target.value
    });
  }


  handlePasswordChange(event) {
    this.setState({
      password: event.target.value
    });
  }


  handlePasswordConfirmationChange(event) {
    this.setState({
      password_conf: event.target.value
    });
  }

  render() {
    if (this.state.redirect) {
      return (<Redirect to="/"/>);
    } else {
    return (
      <main className="row">
        <div className="reg-form col-lg-3">
            <form className="reg-form-fields" action="" method="post">
                <label htmlFor="user-name">Ваше имя:</label>
                <input className="reg-form-item" type="text" id="user-name" placeholder="Введите имя" value={this.state.first_name} onChange={this.handleFirstNameChange}/><br />
                <label htmlFor="user-second-name">Фамилия:</label>
                <input className="reg-form-item" type="text" id="user-second-name" placeholder="Введите фамилию" value={this.state.first_last} onChange={this.handleLastNameChange}/><br />
                <label htmlFor="user-nick-name">Никнейм:</label>
                <input className="reg-form-item" type="text" id="user-nick-name" placeholder="Придумайте ник" value={this.state.nickname} onChange={this.handleNicknameChange}/><br />
                <label htmlFor="user-email">Адрес эл.почты:</label>
                <input className="reg-form-item" type="email" id="user-email" placeholder="Введите e-mail" value={this.state.email} onChange={this.handleEmailChange}/><br />
                <label htmlFor="user-pass">Пароль:</label>
                <input className="reg-form-item" type="password" id="user-pass" placeholder="Придумайте пароль" value={this.state.password} onChange={this.handlePasswordChange}/><br />
                <label htmlFor="user-pass-compare">Подтвердите пароль:</label>
                <input className="reg-form-item" type="password" id="user-pass-compare" placeholder="Подтвердите пароль" value={this.state.password_conf} onChange={this.handlePasswordConfirmationChange}/><br />
                <input className="reg-form-item-btn" type="submit" id="send" name="send" value="Регистрация" onClick={this.handleSubmit}/>
            </form>
        </div>
      </main>
    );
  }
  }
}

export default RegistrationForm;
