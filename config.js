var convict = require('convict');

var config = convict({
	auth: {
		jwt_secret: {
      default: 'wellplayed keyboard cat',
      env: 'JWT_SECRET'
    }
	}
});

config.validate({ allowed: 'strict' });

module.exports = config;
