const socketIO = require('socket.io');
const mongoose = require('mongoose');

module.exports = function (server) {
  const User = mongoose.model('User');
  const Message = mongoose.model('Message');
  const io = socketIO(server);

  io.on('connection', (socket) => {
    console.log('Connected');

    socket.on('authentication', (token) => {
      User.authenticateByToken(token, (err, user) => {
        socket.user = user;
        console.log(`Socket ${socket.id} has authenticated with uid ${user._id}`);

        socket.emit('authenticated', true);
      });
    });

    socket.on('room', (room) => {
      console.log(`Socket ${socket.id} joined room ${room}`);
      socket.room = room;
      socket.join(room);
    });

    socket.on('history', () => {
      Message.find({ room: socket.room })
      .populate('sender')
      .exec()
      .then(messages => {
        socket.emit('history', messages);
      });
    });

    socket.on('chat message', (msg) => {
      var message = new Message({
        room: socket.room,
        sender: socket.user._id,
        content: msg,
        sent: new Date()
      });

      message.save((err, message) => {
        if (err) {
          console.log('Error saving a message');
          console.log('Message: ' + message);
        }
      });

      console.log(socket.user);

      io.in(socket.room).emit('chat message', {
        room: socket.room,
        sent: message.sent,
        content: msg,
        sender: socket.user
      });
    });
  });
};
