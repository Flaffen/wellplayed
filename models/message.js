const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  room: {type: Schema.Types.ObjectId, ref: 'Playground'},
  sender: {type: Schema.Types.ObjectId, ref: 'User'},
  content: String,
  sent: Date
});

module.exports = mongoose.model('Message', MessageSchema);
