const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlaygroundSchema = new Schema({
  address: String,
  position: {
    lat: Number,
    lng: Number
  },
  name: String,
  members: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  going: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

PlaygroundSchema.methods.addNewMember = function(user) {
  if (this.members.indexOf(user._id) === -1) {
    this.members.push(user._id);
    return this.save();
  } else {
    return Promise.reject(new Error('You are already a member'));
  }
};

PlaygroundSchema.methods.removeMember = function(user) {
  if (this.members.indexOf(user._id) !== -1) {
    this.members.remove(user._id);
    return this.save();
  } else {
    return Promise.reject(new Error('You are not a member'));
  }
};

PlaygroundSchema.methods.addGoing = function(user) {
  if (this.going.indexOf(user._id) === -1) {
    this.going.push(user._id);
    return this.save();
  } else {
    return Promise.reject(new Error('You are already going'));
  }
};

PlaygroundSchema.methods.removeGoing = function(user) {
  if (this.going.indexOf(user._id) !== -1) {
    this.going.remove(user._id);
    return this.save();
  } else {
    return Promise.reject(new Error('You are not going'));
  }
};

module.exports = mongoose.model('Playground', PlaygroundSchema);
