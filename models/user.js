const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  nickname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  img: {
    type: String,
    default: 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
  },
  password: {
    type: String,
    required: true
  }
});

UserSchema.pre('save', function (next) {
  var user = this;
  bcrypt.hash(user.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }

    user.password = hash;
    next();
  });
});

UserSchema.statics.authenticate = function (credentials, cb) {
  this.findOne({ email: credentials.email }, function (err, user) {
    if (user === null || err) {
      cb(new Error('No such user'));
    } else {
      bcrypt.compare(credentials.password, user.password, function (err, res) {
        if (err) {
          cb(err);
        } else {
          if (!res) {
            console.log('Passwords do not match');
            cb(new Error('Passwords do not match'));
          } else {
            const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);
            cb(null, token);
          }
        }
      });
    }
  });
};

UserSchema.statics.authenticateByToken = function (token, cb) {
  jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
    this.findOne({ _id: decodedToken._id }, '-password', function (err, user) {
      if (err) {
        cb(err);
      } else {
        cb(null, user);
      }
    });
  });
};

UserSchema.statics.getUsers = function () {
  return this.find().select('-password');
};

module.exports = mongoose.model('User', UserSchema);
