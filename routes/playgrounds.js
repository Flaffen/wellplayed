var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var PlaygroundSchema = require('../models/playground');
var Playground = mongoose.model('Playground', PlaygroundSchema);
var User = require('../models/user');

/* 
 * Get all playgrounds with all ObjectId fields populated by
 * corresponding documents.
 *
 * TODO: Move this function into PlaygroundSchema.statics or check out
 * mongoose autopopulate: http://npmjs.com/package/mongoose-autopopulate.
 */
function getPopulatedPlaygrounds() {
  var playgrounds;

  return Playground.find()
    .then(res => {
      playgrounds = res;
      var promises = [];

      playgrounds.forEach(function (playground) {
        promises.push(User.populate(playground.timetable, { path: 'users' }));
      });

      return Promise.all(promises);
    })
    .then(() => {
      return playgrounds;
    });
}

router.use(function (req, res, next) {
  User.findOne({ email: 'jdoe@example.com' })
    .then(user => {
      req.user = user;
      next();
    });
});

router.get('/', function (req, res, next) {
  var p = getPopulatedPlaygrounds();

  p.then(playgrounds => {
    res.json({
      success: true,
      playgrounds: playgrounds
    });
  });
});

router.post('/', function (req, res, next) {
  // TODO: Create a playground.
});

router.get('/:playgroundId', function (req, res, next) {
  var playground;

  Playground.findById(req.params.playgroundId)
    .then(res => {
      playground = res;
      return User.populate(playground.timetable, { path: 'users' });
    })
    .then(() => {
      res.json({
        success: true,
        playground: playground
      });
    });
});

router.post('/:playgroundId/timetable', function (req, res, next) {
  var time = req.body.time;
  var playground;

  Playground.findById(req.params.playgroundId)
    .then(res => {
      playground = res;

      for (let i of playground.timetable) {
        if (i.time === time) {
          i.users.push(req.user._id);
          break;
        }
      }

      return playground.save();
    })
    .then(() => {
      res.json({
        success: true
      });
    });
});

module.exports = router;
