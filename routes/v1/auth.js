const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const User = mongoose.model('User');

router.post('/signup', (req, res, next) => {
  const user = new User({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    nickname: req.body.nickname,
    email: req.body.email,
    password: req.body.password
  });

  user.save(function (err, user) {
    if (err) {
      next(err);
    }

    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);

    res.json({
      success: true,
      token: token
    });
  });
});

router.get('/signin', (req, res, next) => {
  if (!req.query.email || !req.query.password) {
    next(new Error('All fields are required'));
  }

  User.authenticate({
    email: req.query.email,
    password: req.query.password
  }, function (err, token) {
    if (err) {
      next(err);
    } else {
      res.json({
        success: true,
        token: token
      });
    }
  });
});

router.get('/:token', (req, res, next) => {
  User.authenticateByToken(req.params.token, (err, user) => {
    if (err) {
      next(err);
    } else {
      res.json({
        success: true,
        user: user
      });
    }
  });
});

module.exports = router;
