const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const User = mongoose.model('User');

router.use(function (req, res, next) {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, function (err, decodedToken) {
      console.log(decodedToken);

      User.findOne({ _id: decodedToken._id }, function (err, user) {
        req.user = user;
        next();
      });
    });
  } else {
    next(new Error('No token provided'));
  }
});

module.exports = router;
