const express = require('express');
const router = express.Router();

const usersRouter = require('./users');
const playgroundsRouter = require('./playgrounds');
const authRouter = require('./auth');
const authMiddleware = require('./authMiddleware');

router.use('/auth', authRouter);

router.use(authMiddleware);
router.use('/users', usersRouter);
router.use('/playgrounds', playgroundsRouter);

module.exports = router;
