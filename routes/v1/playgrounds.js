const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Playground = mongoose.model('Playground');

router.get('/', (req, res, next) => {
  Playground.find().then(playgrounds => {
    res.json({
      success: true,
      playgrounds: playgrounds
    });
  })
  .catch(err => {
    next(err);
  });
});

router.get('/:playgroundId', (req, res, next) => {
  Playground.findById(req.params.playgroundId).then(playground => {
    if (playground === null) {
      next(new Error('No such playground found'));
    } else {
      res.json({
        success: true,
        playground: playground
      });
    }
  });
});

router.post('/:playgroundId', (req, res, next) => {
  Playground.findById(req.params.playgroundId)
  .then(playground => {
    if (playground === null) {
      return new Error('No such playground');
    } else {
      return playground.addNewMember(req.user);
    }
  })
  .then(playground => {
    res.json({
      success: true,
      playground: playground
    });
  })
  .catch(err => {
    next(err);
  });
});

router.post('/:playgroundId/cancel', (req, res, next) => {
  Playground.findById(req.params.playgroundId)
  .then(playground => {
    if (playground === null) {
      return new Error('No such playground');
    } else {
      return playground.removeMember(req.user);
    }
  })
  .then(playground => {
    res.json({
      success: true,
      playground: playground
    });
  })
  .catch(err => {
    next(err);
  });
});

router.post('/:playgroundId/going', (req, res, next) => {
  Playground.findById(req.params.playgroundId)
  .then(playground => {
    if (playground === null) {
      return new Error('No such playground');
    } else {
      return playground.addGoing(req.user);
    }
  })
  .then(playground => {
    res.json({
      success: true,
      playground: playground
    });
  })
  .catch(err => {
    next(err);
  });
});

router.post('/:playgroundId/going/cancel', (req, res, next) => {
  Playground.findById(req.params.playgroundId)
  .then(playground => {
    if (playground === null) {
      return new Error('No such playground');
    } else {
      return playground.removeGoing(req.user);
    }
  })
  .then(playground => {
    res.json({
      success: true,
      playground: playground
    });
  })
  .catch(err => {
    next(err);
  });
});

router.post('/', (req, res, next) => {
  Playground.create({
    address: req.body.address,
    name: req.body.name
  })
  .then(playground => {
    res.json({
      success: true,
      playground: playground
    });
  })
  .catch(err => {
    next(err);
  });
});

module.exports = router;
