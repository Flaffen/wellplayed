const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const User = mongoose.model('User');

router.get('/', (req, res) => {
  User.getUsers().then(users => res.json({ users: users }));
});

module.exports = router;
